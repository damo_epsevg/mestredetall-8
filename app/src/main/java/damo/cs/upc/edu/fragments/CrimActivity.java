/**
 * HOLA ANDROID
 */


package damo.cs.upc.edu.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import java.util.UUID;

public class CrimActivity extends SingleFragmentActivity {

    public static final Intent getIntent(Activity a){
        Intent intent = new Intent(a, CrimActivity.class);
        return intent;

    }

    @Override
    public  Fragment getInstance() {
          return CrimFragment.getInstance();
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

     @Override
    protected int getContenidorFragmentResId() {
        return R.id.contenidor;
    }
}
